import type { Metadata } from "next"
import { Inter } from "next/font/google"

import Aside from "@/components/layout/aside"
import Header from "@/components/layout/header"
import ProviderTheme from "@/context/ProviderTheme"

import "@/styles/init.scss"

const inter = Inter({ subsets: ["latin"] })

export const metadata: Metadata = {
  title: "Таблица реестров",
  description: "Таблица реестров",
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <html lang="ru">
      <body className={inter.className}>
        <ProviderTheme>
          <Header />
          <Aside />
          {children}
        </ProviderTheme>
      </body>
    </html>
  )
}
