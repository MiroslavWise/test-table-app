import { NextResponse } from "next/server"

import data from "@/app/api/data/table.json"

export async function GET(request: Request, context: any) {
  const { params } = context ?? {}
  const { id } = params ?? {}

  const find = data.results.find((item) => item.id === Number(id))

  if (find) {
    return NextResponse.json(find, { status: 200 })
  }

  return NextResponse.json({ error: "Не найдёно нужного файла" }, { status: 300 })
}
