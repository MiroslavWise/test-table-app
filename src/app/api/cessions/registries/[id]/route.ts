import { NextResponse } from "next/server"

import data from "@/app/api/data/table.json"

export async function DELETE(request: Request, context: any) {
  const { params } = context ?? {}
  const { id } = params ?? {}
  return NextResponse.json({ id })
}
