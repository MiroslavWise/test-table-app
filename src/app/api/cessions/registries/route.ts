import { NextResponse } from "next/server"

export const dynamic = "force-static"

import data from "@/app/api/data/table.json"

export async function GET(request: Request) {
  return NextResponse.json(data)
}
