export default () => (
  <div className="not-fount-container">
    <article>
      <h2>Страница не найдена</h2>
      <p>К сожалению, страницы с данным адресом не существует.</p>
    </article>
  </div>
)
