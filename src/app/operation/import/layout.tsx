import { Metadata } from "next"

export const metadata: Metadata = {
  title: "Импорт",
  description: "Импорт",
}

export default function LayoutImport({ children }: { children: React.ReactNode }) {
  return children
}
