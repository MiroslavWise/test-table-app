import FiltersAndTable from "@/components/pages/operation/import/FiltersAndTable"
import ButtonModalImport from "@/components/pages/operation/import/ButtonModalImport"

export default () => {
  return (
    <main className="page-main-operation-import">
      <div className="header-operation-import">
        <h2>Импорт файлов</h2>
        <ButtonModalImport />
      </div>
      <FiltersAndTable />
    </main>
  )
}
