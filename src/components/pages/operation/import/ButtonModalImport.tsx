"use client"

import { useState } from "react"

import { styled } from "@mui/material/styles"
import Button from "@mui/material/Button"
import Dialog from "@mui/material/Dialog"
import CloudUploadIcon from "@mui/icons-material/CloudUpload"
import FormGroup from "@mui/material/FormGroup"
import FormControlLabel from "@mui/material/FormControlLabel"
import Checkbox from "@mui/material/Checkbox"

const VisuallyHiddenInput = styled("input")({
  clip: "rect(0 0 0 0)",
  clipPath: "inset(50%)",
  height: 1,
  overflow: "hidden",
  position: "absolute",
  bottom: 0,
  left: 0,
  whiteSpace: "nowrap",
  width: 1,
})

function ButtonModalImport() {
  const [open, setOpen] = useState(false)

  function handleClickOpen() {
    setOpen(true)
  }

  function handleClose() {
    setOpen(false)
  }

  return (
    <>
      <Button variant="contained" onClick={handleClickOpen}>
        <span style={{ color: "#fff" }}>Импорт</span>
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        classes={{
          paper: "dialog-modal-import",
        }}
      >
        <div data-header>
          <h3>Импорт файлов</h3>
        </div>
        <Button
          component="label"
          role={undefined}
          variant="contained"
          tabIndex={-1}
          startIcon={<CloudUploadIcon />}
          style={{ color: "#fff" }}
        >
          Загрузить файлы
          <VisuallyHiddenInput type="file" />
        </Button>
        <FormGroup>
          <FormControlLabel control={<Checkbox defaultChecked />} label="Проверить ИНН" />
          <FormControlLabel control={<Checkbox />} label="Проверить адрес" />
        </FormGroup>
        <div data-footer>
          <Button variant="outlined" onClick={handleClose}>
            Отмена
          </Button>
          <Button variant="contained" onClick={handleClose} style={{ color: "#fff" }}>
            Импортировать
          </Button>
        </div>
      </Dialog>
    </>
  )
}

ButtonModalImport.displayName = "ButtonModalImport"
export default ButtonModalImport
