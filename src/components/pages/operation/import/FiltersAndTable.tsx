"use client"

import { useState } from "react"

import Filters from "./Filters"
import TableData from "./Table"

function FiltersAndTable() {
  const [name, setName] = useState("")
  const [status, setStatus] = useState("")

  return (
    <section>
      <Filters {...{ name, setName, status, setStatus }} />
      <TableData name={name} status={status} />
    </section>
  )
}

FiltersAndTable.displayName = "FiltersAndTable"
export default FiltersAndTable
