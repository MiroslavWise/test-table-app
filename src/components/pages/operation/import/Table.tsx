"use client"

import { useEffect, useMemo, useState } from "react"
import axios, { type AxiosResponse } from "axios"
import dayjs from "dayjs"

import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"

import DownloadIcon from "@mui/icons-material/Download"
import DeleteForeverIcon from "@mui/icons-material/DeleteForever"

import type { IDataImport, IPaginateData } from "@/types/import-table"

interface IProps {
  name: string
  status: string
}

function TableData({ name, status }: IProps) {
  const [data, setData] = useState<IDataImport[]>([])

  useEffect(() => {
    try {
      axios
        .get(`${process.env.NEXT_PUBLIC_API || ""}/api/cessions/registries`)
        .then((response: AxiosResponse<IPaginateData<IDataImport>, any>) => {
          if (response?.data) {
            if (Array.isArray(response?.data?.results)) {
              setData(response?.data?.results)
            }
          }
        })
        .catch((error) => console.log("ERROR AXIOS: ", error))
    } catch (e) {
      console.log("ERROR AXIOS: ", e)
    }
  }, [])

  function onUpload(id: number) {
    try {
      axios
        .get(`${process.env.NEXT_PUBLIC_API || ""}/api/cessions/registries/${id}/export`)
        .then((response) => {
          console.log("upload: ", { response })
        })
        .catch((error) => console.log("AXIOS GET DOC: ", error))
    } catch (e) {
      console.log("AXIOS GET DOC: ", e)
    }
  }
  function onDelete(id: number) {
    try {
      axios({
        method: "delete",
        url: `${process.env.NEXT_PUBLIC_API || ""}/api/cessions/registries/${id}/`,
      })
        .then((response) => {
          console.log("delete: ", { response })
        })
        .catch((error) => console.log("AXIOS DELETE: ", error))
    } catch (e) {
      console.log("AXIOS DELETE: ", e)
    }
    setData((prev) => prev.filter((item) => item?.id !== id))
  }

  const results = useMemo(
    () =>
      data
        .filter((item) => (!name.trim() ? true : item?.cession?.name?.toLowerCase()?.includes(name?.toLowerCase()?.trim())))
        .filter((item) => (!status.trim() ? true : item?.status?.name?.toLowerCase()?.includes(status?.trim()?.toLowerCase()))),
    [data, name, status],
  )

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell>Название файла</TableCell>
            <TableCell>Статус</TableCell>
            <TableCell>Дата загрузки</TableCell>
            <TableCell>Кол-во кредитных договоров</TableCell>
            <TableCell>Загруженно пользователем</TableCell>
            <TableCell width="min-content">Действия</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {results.map((row) => (
            <TableRow key={row?.id} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
              <TableCell component="th" scope="row">
                {row?.id}
              </TableCell>
              <TableCell component="th" scope="row">
                {row?.cession?.name}
              </TableCell>
              <TableCell>{row?.status?.name}</TableCell>
              <TableCell>{dayjs(row?.created_at).format("DD.MM.YYYY")}</TableCell>
              <TableCell>{row?.credits_count}</TableCell>
              <TableCell>{row?.created_by?.full_name}</TableCell>
              <TableCell align="right" classes={{ body: "body-table-operation-actions" }} width="min-content">
                <button type="button" onClick={() => onUpload(row?.id || 0)}>
                  <DownloadIcon />
                </button>
                <button type="button" onClick={() => onDelete(row?.id || 0)}>
                  <DeleteForeverIcon color="error" />
                </button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

TableData.displayName = "TableData"
export default TableData
