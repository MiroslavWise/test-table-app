"use client"

import { Dispatch, SetStateAction } from "react"

import Paper from "@mui/material/Paper"
import InputBase from "@mui/material/InputBase"
import IconButton from "@mui/material/IconButton"
import SearchIcon from "@mui/icons-material/Search"

import styles from "./styles/filters.module.scss"

interface IProps {
  name: string
  setName: Dispatch<SetStateAction<string>>
  status: string
  setStatus: Dispatch<SetStateAction<string>>
}

function Filters({ name, setName, status, setStatus }: IProps) {
  return (
    <div className={styles.container}>
      <Paper component="form" sx={{ p: "2px 4px", display: "flex", alignItems: "center", width: 400 }}>
        <IconButton type="button" sx={{ p: "10px" }} aria-label="search">
          <SearchIcon />
        </IconButton>
        <InputBase
          value={name}
          onChange={(event) => setName(event.target.value || "")}
          sx={{ ml: 1, flex: 1 }}
          placeholder="Поиск по названию"
          inputProps={{ "aria-label": "Поиск по таблице (названию)" }}
        />
      </Paper>
      <Paper component="form" sx={{ p: "2px 4px", display: "flex", alignItems: "center", width: 400 }}>
        <IconButton type="button" sx={{ p: "10px" }} aria-label="search">
          <SearchIcon />
        </IconButton>
        <InputBase
          value={status}
          onChange={(event) => setStatus(event.target.value || "")}
          sx={{ ml: 1, flex: 1 }}
          placeholder="Поиск по статусу"
          inputProps={{ "aria-label": "Поиск по таблице (статусу)" }}
        />
      </Paper>
    </div>
  )
}

Filters.displayName = "Filters"
export default Filters
