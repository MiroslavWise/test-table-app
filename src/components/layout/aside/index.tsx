"use client"

import React from "react"
import List from "@mui/material/List"
import ListItemButton from "@mui/material/ListItemButton"
import ListItemIcon from "@mui/material/ListItemIcon"
import ListItemText from "@mui/material/ListItemText"
import Collapse from "@mui/material/Collapse"
import { ExpandLess, ExpandMore, StarBorder } from "@mui/icons-material"
import Link from "next/link"
import { usePathname } from "next/navigation"
import { MENU } from "./constants/menu"

function Aside() {
  const pathname = usePathname()
  const [open, setOpen] = React.useState<string | null>(null)

  const handleClick = (value: string) => {
    setOpen((prev) => (value !== prev ? value : null))
  }

  return (
    <aside>
      <List sx={{ width: "100%", maxWidth: "100%" }} component="nav" aria-labelledby="nested-list-subheader">
        {MENU.map((item, index) =>
          item?.collapse && Array.isArray(item?.collapse) ? (
            <React.Fragment key={`::key::menu::path::collapse::${item?.path}::${index}::`}>
              <ListItemButton onClick={() => handleClick(item.path)} selected={open === item.path}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText primary={item.label} />
                {open === item.path ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>
              <Collapse in={open === item.path} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  {item.collapse.map((child, index_) => (
                    <Link href={{ pathname: child.path }} key={`::key::menu::child::${child.path}::${index_}::${index}`}>
                      <ListItemButton sx={{ pl: 4 }} selected={pathname.includes(child.path)}>
                        <ListItemIcon>{child.icon}</ListItemIcon>
                        <ListItemText primary={child.label} />
                      </ListItemButton>
                    </Link>
                  ))}
                </List>
              </Collapse>
            </React.Fragment>
          ) : (
            <Link href={{ pathname: item.path }} key={`::key::menu::path::${item?.path}::${index}::`}>
              <ListItemButton selected={pathname.includes(item.path)}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText primary={item.label} />
              </ListItemButton>
            </Link>
          ),
        )}
      </List>
    </aside>
  )
}

Aside.displayName = "Aside"
export default Aside
