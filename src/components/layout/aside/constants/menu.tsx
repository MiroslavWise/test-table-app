import React from "react"
import ContactEmergencyIcon from "@mui/icons-material/ContactEmergency"
import LocationCityIcon from "@mui/icons-material/LocationCity"
import SettingsIcon from "@mui/icons-material/Settings"
import MarkAsUnreadIcon from "@mui/icons-material/MarkAsUnread"
import AccountBalanceWalletIcon from "@mui/icons-material/AccountBalanceWallet"
import ScheduleIcon from "@mui/icons-material/Schedule"
import AdjustIcon from "@mui/icons-material/Adjust"

interface IMenu {
  label: string
  path: string
  icon: React.ReactNode
  collapse?: IMenu[]
}

const MENU: IMenu[] = [
  {
    label: "Организации",
    path: "/organizations",
    icon: <LocationCityIcon />,
  },
  {
    label: "Персонал",
    path: "/staff",
    icon: <ContactEmergencyIcon />,
    collapse: [],
  },
  {
    label: "Кредиты",
    path: "/credits",
    icon: <AccountBalanceWalletIcon />,
    collapse: [],
  },
  {
    label: "Корреспонденция",
    path: "/correspondence",
    icon: <MarkAsUnreadIcon />,
    collapse: [],
  },
  {
    label: "Работа",
    path: "/operation",
    icon: <ScheduleIcon />,
    collapse: [
      {
        label: "Аналитика",
        path: "/operation/analytics",
        icon: <AdjustIcon />,
      },
      {
        label: "Договоры",
        path: "/operation/contracts",
        icon: <AdjustIcon />,
      },
      {
        label: "Импорт",
        path: "/operation/import",
        icon: <AdjustIcon />,
      },
    ],
  },
  {
    label: "Настройки",
    path: "/settings",
    icon: <SettingsIcon />,
  },
]

export { MENU }
