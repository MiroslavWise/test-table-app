import Link from "next/link"
import { Avatar } from "@mui/material"
import AccountCircleIcon from "@mui/icons-material/AccountCircle"

import IconLogo from "@/components/icons/IconLogo"

function Header() {
  return (
    <header>
      <Link href={{ pathname: "/" }}>
        <IconLogo />
      </Link>
      <article>
        <p>Мирослав</p>
        <Link href={{ pathname: "/profile" }}>
          <Avatar sx={{ bgcolor: "green" }}>
            <AccountCircleIcon />
          </Avatar>
        </Link>
      </article>
    </header>
  )
}

Header.displayName = "Header"
export default Header
