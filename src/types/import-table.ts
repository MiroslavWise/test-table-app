export interface IPaginateData<T> {
  count: number
  next: string | null
  previous: string | null
  results: T[]
}

export interface IDataImport {
  id: number
  cession: {
    id: number
    assignor: {
      id: number
      short_name: string
      inn: string
    }
    assignee: {
      id: number
      short_name: string
      inn: string
    }
    name: string
    number: string
    date: string
  }
  file: {
    name: string
    url: string
  }
  credits_count: number
  status: {
    code: string
    name: string
    color: string
  }
  created_by: {
    id: number
    username: string
    full_name: string
  }
  created_at: Date | string
}
